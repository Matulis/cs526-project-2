
from math import *
from euclid import *
from omega import *
from cyclops import *

from omegaToolkit import *

from systemClasses import *
from systemParse import *

from utilities import *

from textureSet import *
from multiplesContainer import *

from CoordinateCalculator import CoordinateCalculator

#Setup small multiples containers:
displaySize = getDisplayPixelSize()


AUTOKM = 149597871

#CONSTANTS
DISPLAYWIDTH = 18
DISPLAYHEIGHT = 4

smallMultipleWidth = 2.0
smallMultipleHeight = 0.5

#Fraction of container that should be star
starContainerFraction = 0.05


#SMALL MULTIPLE SCALE CONSTANTS/GLOBALS
ORBITSCALE = 44.0 #Pluto semimajor
SIZESCALE = 4.0 #Jupiter radius

sm_currentOrbitScale = float(ORBITSCALE)
sm_currentSizeScale = float(SIZESCALE)


##### Planet sphere model
scene = getSceneManager()

scene.setBackgroundColor(Color(0, 0, 0, 1))

mi = ModelInfo()
mi.name = "defaultSphere"
mi.path = "sphere.obj"
scene.loadModel(mi)


######Center system node
centerSystem = SceneNode.create('centerSystem')
everything = SceneNode.create('everything')

everything.addChild(centerSystem)


# Create point light for star
light1 = Light.create()
light1.setLightType(LightType.Point)
light1.setColor(Color(1.0, 1.0, 1.0, 1.0))
light1.setPosition(Vector3(0.0, 0.0, 0.0))
light1.setEnabled(True)

everything.addChild(light1)

print displaySize
uim = UiModule.createAndInitialize()
wf = uim.getWidgetFactory()

uim.getUi().setPadding(0)

#Master container
masterMultiples = Container.create(ContainerLayout.LayoutFree, uim.getUi())
masterMultiples.setAutosize(False)
masterMultiples.setSize(Vector2(displaySize[0],displaySize[1]))
masterMultiples.setPadding(0)
masterMultiples.setMargin(0)
masterMultiples.setLayer(WidgetLayer.Back)

#Left Multiples
leftMultiples = Container.create(ContainerLayout.LayoutHorizontal,masterMultiples)
leftMultiples.setPadding(1)
leftMultiples.setMargin(0)

rightMultiples = Container.create(ContainerLayout.LayoutHorizontal,masterMultiples)
rightMultiples.setPadding(1)
rightMultiples.setMargin(0)
rightMultiples.setPosition(Vector2((DISPLAYWIDTH-3*smallMultipleWidth)/DISPLAYWIDTH*displaySize[0],0))

multiplesSides = [leftMultiples,rightMultiples]

allSmallMultiples = []

for side in multiplesSides:

    for i in xrange(0,3):
        column = Container.create(ContainerLayout.LayoutVertical,side)
        column.setAutosize(False)
        column.setSize(Vector2((smallMultipleWidth/DISPLAYWIDTH)*displaySize[0],displaySize[1]))
        column.setPadding(1)
        column.setMargin(0)
        for i in xrange(0,8):
            smallMultiple = MultiplesContainer.create(ContainerLayout.LayoutFree,column)
            smallMultiple.container.setClippingEnabled(True)
            allSmallMultiples.append(smallMultiple)
            smallMultiple.container.setAutosize(False)
            smallMultiple.container.setSize(Vector2((smallMultipleWidth/DISPLAYWIDTH)*displaySize[0], \
                                   (smallMultipleHeight/DISPLAYHEIGHT)*displaySize[1]))
            smallMultiple.container.setStyle('fill: #11111180')

#Load textures
LoadStarTextures("./startextures")
LoadAllPlanetTextures("./sphere_textures","./multiples_textures")

#Import Solar System Data

universe = ParseAll("./system_data")

currentSystem = None

for system in universe.systems:
    if system.name == "Sun":
        currentSystem = system



def createSmallMultiple(parentContainer,system):
#Create sun
    global sm_currentSizeScale
    global sm_currentOrbitScale
    global ORBITSCALE
    global SIZESCALE
    
    parentContainer.system = system
    
    fontsize = 40
    parentSize = parentContainer.container.getSize()
    print system.name
    star = system.stars[0]
    starType = star.spectraltype
    
    starImage = Image.create(parentContainer.container)
    
    if starType.find('A')!=-1 :
        starImage.setData(StarTextures["astar"])
        parentContainer.innerHabitable = 8.5
        parentContainer.outerHabitable = 12.5
    elif starType.find('B')!=-1 :
        starImage.setData(StarTextures["bstar"])
        parentContainer.innerHabitable = None
        parentContainer.outerHabitable = None
    elif starType.find('F')!=-1 :
        starImage.setData(StarTextures["fstar"])
        parentContainer.innerHabitable = 1.5
        parentContainer.outerHabitable = 2.2
    elif starType.find('G')!=-1 :
        starImage.setData(StarTextures["gstar"])
        parentContainer.innerHabitable = 0.95
        parentContainer.outerHabitable = 1.4
    elif starType.find('K')!=-1 :
        starImage.setData(StarTextures["kstar"])
        parentContainer.innerHabitable = 0.38
        parentContainer.outerHabitable = 0.56
    elif starType.find('M')!=-1 :
        starImage.setData(StarTextures["mstar"])
        parentContainer.innerHabitable = 0.08
        parentContainer.outerHabitable = 0.12
    elif starType.find('O')!=-1 :
        starImage.setData(StarTextures["ostar"])
        parentContainer.innerHabitable = None
        parentContainer.outerHabitable = None
        

    starImage.setSize(Vector2(parentSize[0]*starContainerFraction,parentSize[1]))

    starOffset = starContainerFraction * parentSize[0]
    parentContainer.starOffset = starOffset

#Add container for habitable zone:
    parentContainer.habitableContainer = Container.create(ContainerLayout.LayoutFree,parentContainer.container)
    parentContainer.habitableContainer.setAutosize(False)
    parentContainer.habitableContainer.setSize(Vector2(((parentContainer.outerHabitable-parentContainer.innerHabitable)/sm_currentOrbitScale)*parentSize[0],parentSize[1]))
    parentContainer.habitableContainer.setStyle('fill: #00FF0080')
    parentContainer.habitableContainer.setPosition(Vector2((parentContainer.innerHabitable/sm_currentOrbitScale)*parentSize[0]+starOffset,0))

    

#Add labels for small multiples visualization
    textContainer = Container.create(ContainerLayout.LayoutHorizontal,parentContainer.container)
    textContainer.setPosition(Vector2(textContainer.getPosition()[0]+starOffset,textContainer.getPosition()[1]))
    textContainer.setHorizontalAlign(HAlign.AlignLeft)
    textContainer.setMargin(0)
    textContainer.setPadding(0)

    infoLabels = []
    systemName = Label.create(textContainer) #System name
    systemName.setText(system.name)
    infoLabels.append(systemName)

    distanceLabel = Label.create(textContainer) #System distance in lightyears
    try:
        distanceLabel.setText("{0:.2f} ly".format(system.distance*3.26163344)) #Convert to lightyears
        infoLabels.append(distanceLabel)
    except:
        distanceLabel.setText("") #Convert to lightyears
        infoLabels.append(distanceLabel)

    if(system.discoverymethod is not None):
        discoveryLabel = Label.create(textContainer)
        discoveryLabel.setText(system.discoverymethod)
        infoLabels.append(discoveryLabel)

    starLabel = Label.create(textContainer)
    starLabel.setText("Star: "+ starType)
    infoLabels.append(starLabel)

    for label in infoLabels:
        label.setFont('fonts/arial.ttf {0}'.format(fontsize))
        label.setStyleValue('align', 'top-left')
    

#Load planets
    
    for index,planet in enumerate(star.planets):
        planetImage = Image.create(parentContainer.container)
        planetImage.setData(planet.texture.multiplesTexture)
        planetImage.setSize(Vector2(parentSize[1],parentSize[1]))
        planetImage.setScale(planet.radius/sm_currentSizeScale)

        planetImage.setCenter(Vector2((planet.semimajoraxis/sm_currentOrbitScale)*parentSize[0]+starOffset,parentSize[1]/2))
        parentContainer.planets.append(planetImage)

        planetLabel = Label.create(parentContainer.container)
        planetLabel.setText(planet.name)
        planetLabel.setPosition(Vector2(planetImage.getCenter()[0], fontsize*2 if index%2 else parentSize[1]-fontsize*2))
        planetLabel.setFont('fonts/arial.ttf {0}'.format(fontsize))
        parentContainer.planetLabels.append(planetLabel)
    
#Initialize container scaling so it can keep track of it
        parentContainer.SizeScale = sm_currentSizeScale/SIZESCALE
        parentContainer.OrbitScale = sm_currentOrbitScale/ORBITSCALE


##############################################################################
#Create center system

centerSystem.addChild(currentSystem.setupCenterSystem())



###############################################################################

#Create small multiples menu
mm = MenuManager.createAndInitialize()
smMenu = mm.getMainMenu().addSubMenu("Small Multiples")
sizeScaleLabel = smMenu.addLabel("Size Scale")

#sizeUpButton = smMenu.addButton("+","scaleSize(1.01)")
#sizeDownButton = smMenu.addButton("-","scaleSize(0.99)")
#
#orbitScaleLabel = smMenu.addLabel("Orbit Scale")
#orbitUpButton = smMenu.addButton("+","scaleOrbit(1.01)")
#orbitDownButton = smMenu.addButton("+","scaleOrbit(0.99)")

sizeScaleSlider = smMenu.addSlider(22,"sizescale =pow(1.2 if %value%-11>0 else 0.8,abs(%value%-11)); scaleSize(sizescale)")
sizeScaleSlider.getSlider().setValue(11) #Half of slider
sizeScaleSlider.getSlider().setWidth(200)
orbitScaleLabel = smMenu.addLabel("Orbit Scale")
orbitScaleSlider = smMenu.addSlider(22,"oscale =pow(1.2 if %value%-11>0 else 0.8,abs(%value%-11));scaleOrbit(oscale)")
orbitScaleSlider.getSlider().setValue(11) #Half of slider
orbitScaleSlider.getSlider().setWidth(200)


resetButton = smMenu.addButton("Reset Small Multiples","resetSmallMultiples()")


#Create center system menu
cMenu = mm.getMainMenu().addSubMenu("Center System")
sizeScaleLabel = cMenu.addLabel("Size Scale")

#sizeUpButton = smMenu.addButton("+","scaleSize(1.01)")
#sizeDownButton = smMenu.addButton("-","scaleSize(0.99)")
#
#orbitScaleLabel = smMenu.addLabel("Orbit Scale")
#orbitUpButton = smMenu.addButton("+","scaleOrbit(1.01)")
#orbitDownButton = smMenu.addButton("+","scaleOrbit(0.99)")

sizeScaleSlider = cMenu.addSlider(22,"scale = pow(1.5,%value%-11); delete(centerSystem); centerSystem.addChild(currentSystem.setupCenterSystem(scale,None))")
sizeScaleSlider.getSlider().setValue(11) #Half of slider
sizeScaleSlider.getSlider().setWidth(200)
orbitScaleLabel = cMenu.addLabel("Orbit Scale")
orbitScaleSlider = cMenu.addSlider(22,"scale = pow(1.5,%value%-11); delete(centerSystem); centerSystem.addChild(currentSystem.setupCenterSystem(None,scale))")
orbitScaleSlider.getSlider().setValue(11) #Half of slider
orbitScaleSlider.getSlider().setWidth(200)

timeScaleLabel = cMenu.addLabel("Time Scale Slider")
timeScaleSlider = cMenu.addSlider(24,"timeScale = pow(2,%value%-12)")
timeScaleSlider.getSlider().setValue(12) #Half of slider
timeScaleSlider.getSlider().setWidth(200)

resetButton = cMenu.addButton("Reset Size+Orbit+Camera","resetCenterAndCamera()")

universeTime = 0
timeScale = 1

def onUpdate(frame, t, dt):
    global universeTime
    global timeScale
    universeTime += dt*timeScale
    if centerSystem.numChildren()>0:
        for planet in currentSystem.stars[0].planets:
            try:
                pos = get2DPositionofPlanet(planet,universeTime)
                planet.sceneGraphNode.setPosition(Vector3(pos[0],0,pos[1]))
                if planet.day != None:
                    planet.modelNode.yaw(math.radians(dt*timeScale*planet.day))
            except: pass

            
setUpdateFunction(onUpdate)

def areAllPlanetsVisible(multiples_container):
    allPlanetsVisible = True
    for planet in multiples_container.planets:
        if(planet.getCenter()[0] > multiples_container.container.getSize()[0]):
            allPlanetsVisible = False
            
    if allPlanetsVisible is True:
        multiples_container.container.setStyleValue('border', '0 #0000FF')
    else:
        multiples_container.container.setStyleValue('border', '5 #0000FF')

def resetSmallMultiples():
    for container in allSmallMultiples:
        for planet in container.planets:
            try: float(container.SizeScale)
            except: container.SizeScale = 1
            planet.setScale(planet.getScale()*(1/container.SizeScale))
            planet.setCenter(Vector2(planet.getCenter()[0]*(1/container.OrbitScale),planet.getCenter()[1]))
        
        for label in container.planetLabels:
            label.setPosition(Vector2(label.getPosition()[0] * (1/container.OrbitScale),label.getPosition()[1]))
    
        if container.habitableContainer is not None:
            container.habitableContainer.setPosition(Vector2(container.habitableContainer.getPosition()[0]*(1/container.OrbitScale),0))
            container.habitableContainer.setSize(Vector2(container.habitableContainer.getSize()[0]*(1/container.OrbitScale),container.habitableContainer.getSize()[1]))

        areAllPlanetsVisible(container)
        container.SizeScale = 1
        container.OrbitScale = 1
            
    sm_currentSizeScale = SIZESCALE
    sm_currentOrbitScale = ORBITSCALE

def resetCenterAndCamera():
    global centerScaleFactor
    global centerOrbitFactor

    centerScaleFactor = 0.3
    centerOrbitFactor = 0.3
    centerSystem.setChildrenVisible(False)
    delete(centerSystem);
    centerSystem.addChild(currentSystem.setupCenterSystem(centerScaleFactor,centerOrbitFactor))

def updateCenter():
    centerSystem.setChildrenVisible(False)
    delete(centerSystem);
    centerSystem.addChild(currentSystem.setupCenterSystem(None,None))

#    getDefaultCamera().setPosition(Vector3(0,0,0))

#Move camera to default



def scaleOrbit(scale):
    global allSmallMultiples
    
    sm_currentOrbitScale = scale*ORBITSCALE
    for container in allSmallMultiples:
        print len(container.planets)
        for planet in container.planets:
            try: float(container.OrbitScale)
            except: container.OrbitScale = 1
            planet.setCenter(Vector2(planet.getCenter()[0] * (scale/container.OrbitScale),planet.getCenter()[1]))
        for label in container.planetLabels:
            label.setPosition(Vector2(label.getPosition()[0] * (scale/container.OrbitScale),label.getPosition()[1]))
    
        if container.habitableContainer is not None:
            container.habitableContainer.setSize(Vector2( (container.habitableContainer.getSize()[0]*(float(scale)/float(container.OrbitScale))),container.container.getSize()[1]))

            container.habitableContainer.setPosition(Vector2(container.habitableContainer.getPosition()[0]*(float(scale)/float(container.OrbitScale)),0))
#            container.habitableContainer.setPosition(Vector2( (container.innerHabitable*container.container.getSize()[0]/scale+container.starOffset,0))
            

        areAllPlanetsVisible(container)
        container.OrbitScale = float(scale)

#            parentContainer.habitableContainer.setSize(Vector2(((parentContainer.outerHabitable-parentContainer.innerHabitable)/sm_currentOrbitScale)*parentSize[0]+starOffset,parentSize[1]))
#                parentContainer.habitableContainer.setStyle('fill: #00FF0080')



def scaleSize(scale):
    sm_currentSizeScale = scale*SIZESCALE
    
    for container in allSmallMultiples:
        for planet in container.planets:
            try: float(container.SizeScale)
            except: container.SizeScale = 1
            planet.setScale(planet.getScale()*(scale/container.SizeScale))
        areAllPlanetsVisible(container)
        container.SizeScale = scale
#initialize planets

for index,container in enumerate(allSmallMultiples):
    createSmallMultiple(container,universe.systems[index])
    areAllPlanetsVisible(container)


#masterMultiples.setVisible(False)


oscale = 1
sizescale = 1

####################  Music    ########################
playMusic = True
env = getSoundEnvironment()
env.setAssetDirectory("/home/evl/cs526/martin/Project2/music/")
if playMusic:
    music = env.loadSoundFromFile('music', 'track1.wav') #might need full path here?
    simusic = SoundInstance(music)
    simusic.setVolume(.2)
    simusic.setLoop(True)
    simusic.play()


# Add keys to remove / add the entity to the clip plane
def onEvent():
    global oscale
    global sizescale
    global timeScale
    global currentSystem
    
    e = getEvent()


    if(e.isKeyDown(ord('o'))):
        sizescale *= 1.01
        scaleSize(sizescale)
    if(e.isKeyDown(ord('l'))):
        sizescale *= 0.99
        scaleSize(sizescale)
#    if(e.isKeyDown(ord('p'))):
#        timeScale *= 2.0
#        print timeScale
#    if(e.isKeyDown(ord('i'))):
#        timeScale *= 1/2.0
#        print timeScale

    if(e.isKeyDown(ord('p'))):
        oscale *= 1.01
        scaleOrbit(oscale)
    if(e.isKeyDown(ord('i'))):
        oscale *= 0.99
        scaleOrbit(oscale)

    if(e.isKeyDown(ord('t'))):
        resetCenterAndCamera()

    if(e.isButtonDown(EventFlags.Button5) or e.isButtonDown(EventFlags.Button1)):
        
        print "Button 5 Clicked"
        screenPosition = CoordinateCalculator()
        wand_pos = e.getPosition()
        wand_orient = e.getOrientation()
        r = Vector3(0.0,0.0,-1.0)
        v = wand_orient * r
        screenPosition.set_position(wand_pos.x, wand_pos.y, wand_pos.z)
        screenPosition.set_orientation(v.x, v.y, v.z)
        screenPosition.calculate()
        x = screenPosition.get_x() * displaySize[0]
        y = screenPosition.get_y() * displaySize[1]
        print "X:" , x
        print "Y:" , y

        for container in allSmallMultiples:
            if(container.container.isEventInside(e)):
                currentSystem = container.system
                print currentSystem.name
                updateCenter()
                break
            if(container.container.hitTest(Vector2(x,y))):
                currentSystem = container.system
                print currentSystem.name
                updateCenter()
                break

            

setEventFunction(onEvent)