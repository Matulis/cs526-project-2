def delete(node):
    numChildren = node.numChildren()
    for i in xrange(numChildren):
        child = node.getChildByIndex(0)
        delete(child)
        node.removeChildByIndex(0)

