
var M_PI 	= 3.1415926535897; // C constant
var scale 	= 0;
var width 	= 400;
var height 	= 300;
var planets;
var speed 	= 0.5;
var minperiod 	= 100000000;
var zoom 	= 0;
var t 		= 0;


def get2DPositionofPlanet(planet,time):
    longitude = radians(float(planet.longitude))
	ascendingnode = radians(float(planet.ascendingnode))
	semimajoraxis = float(planet.semimajoraxis)
	periastron = radians(float(planet.periastron))
	period = float(planet.period)
	eccentricity = float(planet.eccentricity)
	
	meanAnomaly = longitude - periastron
	if period:
		meanAnomaly += (2 * math.pi *time) /period
	else:
		meanAnomaly += (2 * math.pi *time) /365
		
	eccentricAnomaly = meanAnomaly + eccentricity*math.sin(meanAnomaly)
	deltaE = 0
	counter = 15
	
	while True:
		deltaE = (eccentricAnomaly - eccentricity*math.sin(eccentricAnomaly)- meanAnomaly )/(1.-eccentricity*math.cos(eccentricAnomaly));
		eccentricAnomaly -= deltaE;
		counter--;	
		if math.abs(deltaE)>1e-4 and counter>0:
			continue
		else:
			break
	
	_a = 1
	if (semimajoraxis>0):
		_a = semimajoraxis
	
	xp =_a*(math.cos(eccentricAnomaly)-eccentricity)
	yp =_a*math.sqrt(1.-eccentricity*eccentricity) * math.sin(eccentricAnomaly)
	
	sinNodengle = math.sin(ascendingnode)
	cosNodengle = math.cos(ascendingnode)
	sinArgPeri = math.sin(periastron-ascendingnode)
	cosArgPeri = math.cos(periastron-ascendingnode)
	
	x = xp*(cosArgPeri*cosNodengle-sinArgPeri*sinNodengle) + yp*(-sinArgPeri*cosNodengle-cosArgPeri*sinNodengle)
	y = xp*(cosArgPeri*sinNodengle+sinArgPeri*cosNodengle) + yp*(-sinArgPeri*sinNodengle+cosArgPeri*cosNodengle)
	
	x = x * scale * math.pow(10,zoom);
	y = y * scale * math.pow(10,zoom);
	
	point = [x,y]
	return point;
		
		
def getPathForPlanet(planet):
	N = 64 #number of segments
	thickness = 0.01
	pointOld = get2DPositionofPlanet(planet,0)
	
	orbit = LineSet.create()
	
	for i in xrange(N):
		pointNew = get2DPositionofPlanet(planet,period*i/N)
		#Draw line segment
		l = circle.addLine()
        l.setStart(Vector3(pointOld[0], pointOld[1],0))
        l.setEnd(Vector3(pointNew[0], pointNew[1],0))
        l.setThickness(thickness)
		pointOld = pointNew
		
	orbit.setEffect('colored -e white')
	
def setupOrbitPlot(system):

	nodeOrbits = SceneNode.create("SmallOrbit"+system.name)
	
	for star in stars:
		for planet in planets:
			a = planet.semimajoraxis
			eccentricity = float(planet.eccentricity)
			period = float(planet.period)
			starmass = planet.parentNode.mass
			if period <= 0 and a > 0:
				if(starmass > 0):
					period = 2* math.pi * math.sqrt(a*a*a*3378.2183/starmass)
				else:
					period = 2* math.pi * math.sqrt(a*a*a*3378.2183)
				
			if a <= 0 and period > 0:
				if(starmass = 0):
					a = math.pow(period*period/4./math.pi/math.pi*0.00029601403*starmass, 1./3.)
				else:
					a = math.pow(period*period/4./math.pi/math.pi*0.00029601403, 1./3.) #Assume solar mass
				planet.semimajoraxis = a
				
			nodeOrbits.addChild(getPathForPlanet(planet))
	
			
			
			
			
			
			
			