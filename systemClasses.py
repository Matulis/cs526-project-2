from math import *
from euclid import *
from omega import *
from cyclops import *

from textureSet import *
import random

jupiterRadiustoKm = 69911
solarRadiustoKm = 696000

centerScaleFactor = 0.3
centerOrbitFactor = 0.3
t = 0

orbitColors = ["red","aqua","orange","purple","yellow","silver","green","purple","silver","red"]

class Universe():
    systems = []
    
class Planet():

    def __init__(self):
        self.name = None
        self.mass = None #Jupiter masses
        self.radius = None #jupiter radii
        self.temperature = None
        self.period = None #day
        self.semimajoraxis = None #AU
        self.eccentricity = None
        self.inclination = None #degree
        self.periastron = None #degree
        self.longitude = None #degree
        self.discoverymethod = None
        self.parentNode = None
        self.texture = None
        self.ascendingnode = None
        self.day = None
        self.tilt = None
        
        self.sceneGraphNode = None
        self.modelNode = None


class Star():
    def __init__(self):
        self.name = None
        self.mass = None #solar mass
        self.radius = None #solar radii
        self.temperature = None
        self.spectraltype = None
        self.day = None
        self.tilt = None
        
        self.parentNode = None
        self.sceneGraphNode = None
        self.modelNode = None
        
        self.planets = []
    
    def addPlanet(self,planet):
        #TODO add to scene graph node
        self.planets.append(planet)

    def getSizeSpheres(self):
        sizeSpheres = []
        sphere = SizeSphere(self.name,self.radius*solarRadiustoKm)
        sizeSpheres.append(sphere)
        for planet in self.planets:
            sphere = SizeSphere(planet.name,planet.radius*jupiterRadiustoKm,planet.textureFilepath)
            sizeSpheres.append(sphere)
        return sizeSpheres


class Binary():
    
    def __init__(self):
    
        self.semimajoraxis = None #AU
        self.eccentricity = None
        self.periastron = None
        self.longitude = None
        self.ascendingnode = None
        self.period = None
        self.parentNode = None
        self.sceneGraphNode = None
        
        self.planets = []
        self.stars = []
    
    def addStar(self,star):
        #TODO add to scene graph node
        self.stars.append(star)
        
    def addPlanet(self,planet):
        #TODO add to scene graph node
        self.planets.append(planet)

    def getSizeSpheres(self):
        sizeSpheres = []
        for planet in planets:
            sphere = SizeSphere(planet.name,plaent.radius*jupiterRadiustoKm,planet.textureFilepath)
            sizeSpheres.append(sphere)
        for star in stars:
            sizeSpheres = sizeSpheres +  star.getSizeSpheres()
        return sizeSpheres
    

class System():
    
    def __init__(self):
    
        self.name = None
        self.rightascension = None #hh mm ss
        self.declination = None #dd mm ss
        self.distance = None #parsec
        self.sceneGraphNode = None
        self.discoverymethod = None
        
        self.orphanPlanets = []
        self.stars = []
        self.binaries = []
    
    def addStar(self,star):
        #TODO add to scene graph node
        self.stars.append(star)
        
    def addPlanet(self,planet):
        #TODO add to scene graph node
        self.orphanPlanets.append(planet)
        
    def addBinary(self,binary):
        #TODO add to scene graph node
        self.binaries.append(binary)
    
    def getSizeSpheres(self):
        sizeSpheres = []
        for planet in self.orphanPlanets:
            sphere = SizeSphere(planet.name,planet.radius*jupiterRadiustoKm,planet.textureFilepath)
            sizeSpheres.append(sphere)
        for star in self.stars:
            sizeSpheres = sizeSpheres + star.getSizeSpheres()
        for binary in self.binaries:
            sizeSpheres = sizeSpheres + binary.getSizeSpheres()

        return sizeSpheres



    def getSizeSmallMultiple(self,smallestToBiggest=False):
        textsize = 0.05
        node = SceneNode.create("PlanetSmallMultiples"+self.name)

        sizeSpheres = self.getSizeSpheres()
        if(smallestToBiggest):
            #Sort all the SizeSpheres from smallest to biggest
            sizeSpheres = sorted(sizeSpheres,key=lambda sizeSphere : sizeSphere.radius)
        print len(sizeSpheres)
        maxRadius = sorted(sizeSpheres,key=lambda sizeSphere : sizeSphere.radius)[len(sizeSpheres)-1].radius

        xOffset = 0
        for sphere in sizeSpheres:
        #Draw spheres here
            
            radius = sphere.radius/maxRadius
            print radius
            s = SphereShape.create(radius,4)
            offset = max(radius,textsize)
            s.setPosition(Vector3(xOffset+offset,radius,0))
            t = Text3D.create('fonts/arial.ttf', textsize, sphere.name)
            t.setPosition(Vector3(0,-radius-textsize*3,0))
            t.roll(math.radians(-45))
            s.addChild(t)
            node.addChild(s)

            xOffset = xOffset + textsize + 2*radius
                
                #possible check to see if spheres should not be drawn?
                #scale Z axis down?

        return node

#    def getOrbitSmallMultiple(self):
#        textsize = 0.05
#        #edit to actually use real data?
#        #use globals to possibly not draw parts?
#        #add text
#        
#        node = SceneNode.create("PlanetSmallMultiples"+self.name)
#        
#        sizeSpheres = self.getSizeSpheres()
#        
#        maxRadius = sorted(sizeSpheres,key=lambda sizeSphere : sizeSphere.radius)[len(sizeSpheres)-1].radius
#        
#        for sphere in sizeSpheres:
#            #Draw spheres here
#            radius = sphere.radius/maxRadius
#            print radius
#            s = addOrbit(radius,0.01)
#            t = Text3D.create('fonts/arial.ttf', textsize, sphere.name)
#            t.setPosition(Vector3(0,-1,0))
##            t.roll(math.radians(-45))
#            s.addChild(t)
#
#            node.addChild(s)
#        
#        #possible check to see if spheres should not be drawn?
#        #scale Z axis down?
#        
#        return node

    def getOrbitSmallMultiple(self):
        
        #TODO ADD System Information
        #TODO verify other systems
        textsize = 30
        planetSize = 0.04
        
        node = SceneNode.create("PlanetSmallOrbital"+self.name)
        
        for star in self.stars:
            starBall = SphereShape.create(planetSize,4)
            starBall.getMaterial().setColor(Color("yellow"),Color("yellow"))
            
            #Sun text
            t = Text3D.create('fonts/arial.ttf', textsize, star.name)
            t.setPosition(Vector3(0,0.1,0))
            t.setFixedSize(True)
            starBall.addChild(t)
            
            for index,planet in enumerate(star.planets):
                s = addOrbit(planet.semimajoraxis,0.01)
                s.getMaterial().setColor(Color(orbitColors[index]),Color(orbitColors[index]))

#                #Planet visualization
#                print planet.name , planet.semimajoraxis
#                ball = SphereShape.create(planetSize,4)
#                ball.setPosition(Vector3(planet.semimajoraxis,0,0))
#                ball.getMaterial().setColor(Color(orbitColors[index]),Color(orbitColors[index]))
##                s.addChild(ball)
#                
#                #Planet text
#                t = Text3D.create('fonts/arial.ttf', textsize, planet.name)
#                t.setPosition(Vector3(0,0.1,0))
#                t.setFixedSize(True)
#                ball.addChild(t)

                starBall.addChild(s)
                starBall.addChild(ball)
    
            node.addChild(starBall)

        return node


    def setupCenterSystem(self,sizeScaleValue=None,orbitScaleValue=None):
        global centerScaleFactor
        global centerOrbitFactor
        
        
        if sizeScaleValue is not None:
            centerScaleFactor = sizeScaleValue
        
        if orbitScaleValue is not None:
            centerOrbitFactor = orbitScaleValue
        print "STart"
        print centerOrbitFactor
        print centerScaleFactor
        print "End"
                
        planetSize = 0.5
        planetCenterLabelSize = 0.2
        textsize = 30
        nodeOrbits = SceneNode.create("SmallOrbit"+self.name)
        

        for star in self.stars:
            
            model = StaticObject.create("defaultSphere")
            model.setPosition(Vector3(0,0,0))
            model.setScale(Vector3(0.01,0.01,0.01))
            
            model.getMaterial().setProgram('textured emissive')
           
            
            starType = star.spectraltype
            starTexturePath = ""
            if starType.find('A')!=-1 :
                starTexturePath=StarSphereTextures["astar"]
            elif starType.find('B')!=-1 :
                starTexturePath=StarSphereTextures["bstar"]
            elif starType.find('F')!=-1 :
                starTexturePath=StarSphereTextures["fstar"]
            elif starType.find('G')!=-1 :
                starTexturePath=StarSphereTextures["gstar"]
            elif starType.find('K')!=-1 :
                starTexturePath=StarSphereTextures["kstar"]
            elif starType.find('M')!=-1 :
               starTexturePath=StarSphereTextures["mstar"]
            elif starType.find('O')!=-1 :
               starTexturePath=StarSphereTextures["ostar"]
            
            
            print starType
            model.getMaterial().setDiffuseTexture(starTexturePath)
        
            # deal with the axial tilt of the sun
            if star.tilt is not None:
                tiltCenter = SceneNode.create(star.name+"TiltCenter")
                nodeOrbits.addChild(tiltCenter)
                tiltCenter.addChild(model)
                tiltCenter.roll(math.radians(star.tilt))
        
            for index,planet in enumerate(star.planets):
                a = planet.semimajoraxis
                try:    eccentricity = float(planet.eccentricity)
                except:
                    eccentricity = 0
                    planet.eccentricity = 0
                period = float(planet.period)
                starmass = planet.parentNode.mass
                if period <= 0 and a > 0:
                    if(starmass > 0):
                        period = 2* math.pi * math.sqrt(a*a*a*3378.2183/starmass)
                    else:
                        period = 2* math.pi * math.sqrt(a*a*a*3378.2183)
                
                if a <= 0 and period > 0:
                    if(starmass == 0):
                        a = math.pow(period*period/4./math.pi/math.pi*0.00029601403*starmass, 1./3.)
                    else:
                        a = math.pow(period*period/4./math.pi/math.pi*0.00029601403, 1./3.) #Assume solar mass
                    planet.semimajoraxis = a
                orbit = getPathForPlanet(planet)
                nodeOrbits.addChild(orbit)
                print planet.name
                print index
                orbit.getMaterial().setColor(Color(orbitColors[index]),Color(orbitColors[index]))
#                #Planet visualization
                model = StaticObject.create("defaultSphere")
                planet.sceneGraphNode = model
                planet.modelNode = model
            
                #Planet text
                
                t = Text3D.create('fonts/arial.ttf', planetCenterLabelSize, planet.name)
                t.setPosition(Vector3(0,model.getScale()[1]*1.5,0))
                t.setFacingCamera(getDefaultCamera())
                
                # deal with the axial tilt of the planets
                if planet.tilt is not None:
                    tiltCenter = SceneNode.create(planet.name+"TiltCenter")
                    nodeOrbits.addChild(tiltCenter)
                    tiltCenter.addChild(model)
                    tiltCenter.roll(math.radians(planet.tilt))
#                    rotationCorrectNode.setRotation(tiltCenter.getRotation())
                    tiltCenter.addChild(t)
                    planet.sceneGraphNode = tiltCenter
                else:
                    nodeOrbits.addChild(model)
                    model.addChild(t)
                
                position = get2DPositionofPlanet(planet,0)
                planet.sceneGraphNode.setPosition(Vector3(position[0],0,position[1]))
                planet.sceneGraphNode.setScale(Vector3(planet.radius*centerScaleFactor,planet.radius*centerScaleFactor,planet.radius*centerScaleFactor))
                print centerScaleFactor
                model.getMaterial().setProgram('textured')
                model.getMaterial().setDiffuseTexture(planet.texture.sphereTexture)
                    
               

#
#                nodeOrbits.addChild(ball)
    
        return nodeOrbits




class SizeSphere():
    name = None
    textureFilepath = None
    radius = None
    def __init__(self,name,radius,textureFilepath=None):
        self.name = name
        self.radius = float(radius)
        self.textureFilepath = textureFilepath
    
def addOrbit(orbit, thick):
    circle = LineSet.create()
    
    segments = 128
    radius = 1
    thickness = thick   #0.01 for orbit
    
    a = 0.0
    while a <= 360:
        x = cos(radians(a)) * radius
        y = sin(radians(a)) * radius
        a += 360.0 / segments
        nx = cos(radians(a)) * radius
        ny = sin(radians(a)) * radius
        
        l = circle.addLine()
        l.setStart(Vector3(x, y,0))
        l.setEnd(Vector3(nx,ny,0))
        l.setThickness(thickness)
        
    circle.setEffect('colored -e white')
        
#        if col == 0:
#            circle.setEffect('colored -e white')
#        else:
#            circle.setEffect('colored -e green')
        
        # Squish z to turn the torus into a disc-like shape.
        
    circle.setScale(Vector3(orbit, orbit, 1)) # 0.1
        
        
    return circle



def get2DPositionofPlanet(planet,time):
    global centerScaleFactor
    global centerOrbitFactor
    
    longitude = math.radians(planet.longitude or 0)
    try: ascendingnode = math.radians(float(planet.ascendingnode))
    except: ascendingnode = 0
    semimajoraxis = float(planet.semimajoraxis)
    try: periastron = math.radians(float(planet.periastron))
    except: periastron = 0
    period = float(planet.period)
    try:    eccentricity = float(planet.eccentricity)
    except:
        eccentricity = 0
        planet.eccentricity = 0
    
    meanAnomaly = longitude - periastron
    if period:
        meanAnomaly += (2 * math.pi *time) /period
    else:
        meanAnomaly += (2 * math.pi *time) /365
    
    eccentricAnomaly = meanAnomaly + eccentricity*math.sin(meanAnomaly)
    deltaE = 0
    counter = 15
    
    while True:
        deltaE = (eccentricAnomaly - eccentricity*math.sin(eccentricAnomaly)- meanAnomaly )/(1.-eccentricity*math.cos(eccentricAnomaly))
        eccentricAnomaly -= deltaE
        counter -= 1
        if abs(deltaE)>1e-4 and counter>0:
            continue
        else:
            break
    
    _a = 1
    if (semimajoraxis>0):
        _a = semimajoraxis
    
    xp =_a*(math.cos(eccentricAnomaly)-eccentricity)
    yp =_a*math.sqrt(1.-eccentricity*eccentricity) * math.sin(eccentricAnomaly)
    
    sinNodengle = math.sin(ascendingnode)
    cosNodengle = math.cos(ascendingnode)
    sinArgPeri = math.sin(periastron-ascendingnode)
    cosArgPeri = math.cos(periastron-ascendingnode)
    
    x = xp*(cosArgPeri*cosNodengle-sinArgPeri*sinNodengle) + yp*(-sinArgPeri*cosNodengle-cosArgPeri*sinNodengle)
    y = xp*(cosArgPeri*sinNodengle+sinArgPeri*cosNodengle) + yp*(-sinArgPeri*sinNodengle+cosArgPeri*cosNodengle)
    
    x = x  * centerOrbitFactor;
    y = y  * centerOrbitFactor;
    
    point = [x,y]
    return point;


def getPathForPlanet(planet):
    N = 64 #number of segments
    thickness = 0.2 * planet.radius * centerScaleFactor
    pointOld = get2DPositionofPlanet(planet,0)
    
    orbit = LineSet.create()
    
    for i in xrange(1,N+1):
        pointNew = get2DPositionofPlanet(planet,planet.period*i/N)
        #Draw line segment
        l = orbit.addLine()
        l.setStart(Vector3(pointOld[0],0,pointOld[1]))
        l.setEnd(Vector3(pointNew[0],0,pointNew[1]))
        l.setThickness(thickness)
        
        pointOld = pointNew

    return orbit











