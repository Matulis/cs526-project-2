
from math import *
from euclid import *
from omega import *
from cyclops import *

from omegaToolkit import *

class MultiplesContainer(Container):
        
    def __init__(self,ContainerLayout,parentContainer):
        self.planets = [] #Store planet images to be scaled
        self.planetLabels = []
        self.innerHabitable = None
        self.outerHabitable = None
        self.habitableContainer = None
        self.container = None
        self.SizeScale = None
        self.OrbitScale = None
        self.starOffset = None
        self.system = None
        
        self.container = Container.create(ContainerLayout,parentContainer)

    @staticmethod
    def create(ContainerLayout,parentContainer):
        return MultiplesContainer(ContainerLayout,parentContainer)