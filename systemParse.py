from systemClasses import *
from textureSet import *

import os
import xml.etree.ElementTree as ET, glob
import random

# Some Parsing Code Adapted From " A PROPOSAL FOR COMMUNITY DRIVEN AND DECENTRALIZED ASTRONOMICAL DATABASES
# AND THE OPEN EXOPLANET CATALOGUE"


def ParseAll(filepath):
    #iterate through XML files in system_data folder
    
    universe = Universe()
    
    # filenames = os.listdir(filepath)
    filenames = glob.glob(filepath+"/*.xml")
    for filename in filenames:
        universe.systems.append(ParseSystem(filename))

    print "Universe size(number of systems): " + str(len(universe.systems))

    return universe

def ParseSystem(filepath):
    #open file and parse
    tree = ET.parse(open(filepath,"r"))
    root = tree.getroot()

    system = System()
    system.name = root.find("name").text
    print "Creating System: " + system.name
    
    try:
        system.discoverymethod = root.findall(".//discoverymethod")[0].text
    except:
        system.discoverymethod = ""

    try: system.rightascension = root.find("rightascension").text
    except: pass
    try: system.declination = root.find("declination").text
    except: pass
    try: system.distance = float(root.find("distance").text)
    except: pass
    
    planets = root.findall("planet")
    for planet in planets:
        lonePlanet = ParsePlanetElement(planet)
        lonePlanet.parentNode = system
        system.orphanPlanets.append(lonePlanet)
        print "Found a lone planet "

    stars = root.findall("star")
    for star in stars:
        starObject = ParseStarElement(star)
        starObject.parentNode = system
        system.stars.append(starObject)

    binaries = root.findall("binary")
    for binary in binaries:
        binaryObject = ParseBinaryElement(binary)
        binaryObject.parentNode = system
        system.binaries.append(binaryObject)

    return system

def ParsePlanetElement(root):
    planet = Planet()
    try: planet.name = root.find("name").text.split()[-1]
    except: pass
    
    print planet.name

    try:planet.mass = float(root.find("mass").text)
    except: pass

    try: planet.radius = float(root.find("radius").text)
    except:
    #Calculate earth radius from mass(at end converted to jupiter)
        earthmass = planet.mass * 317.828133 #Convert jupiter mass to earth mass
        if(earthmass < 2):
            radius = (earthmass/2)*1.25
        elif(earthmass>=2 and earthmass<5):
            radius = ((earthmass/3)*5)+5
        elif(earthmass >=5 and earthmass<10):
            radius = ((earthmass/5)*1)+2
        elif(earthmass >=10 and earthmass<30):
            radius = ((earthmass/20)*3)+3
        elif(earthmass >=30 and earthmass<300):
            radius = ((earthmass/270)*9)+6
        else:
            radius = 15 * (earthmass/300)
        
        planet.radius = radius * 0.09113510278
        
    try: planet.day = float(root.find("day").text)
    except:pass
        
    try: planet.tilt = float(root.find("tilt").text)
    except:pass

    try: planet.temperature = root.find("temperature").text
    except:pass

    try: planet.period = float(root.find("period").text)
    except:pass

    try: planet.semimajoraxis = float(root.find("semimajoraxis").text)
    except:pass

    try: planet.eccentricity = root.find("eccentricity").text
    except: pass
    
    try: planet.inclination = root.find("inclination").text
    except: pass
    
    try: planet.periastron = root.find("periastron").text
    except: pass
    
    try: planet.longtitude = float(root.find("longitude").text)
    except: pass
    
    try: planet.ascendingnode = float(root.find("ascendingnode").text)
    except: pass
    
    try: planet.discoverymethod = root.find("discoverymethod").text
    except: pass
    
    random.seed(planet.mass)
    #planet texture
    if planet.name.lower() in PlanetTextures:
        planet.texture = PlanetTextures[planet.name.lower()]
    else:
        planet.texture = PlanetTextures[str(random.randint(1,NUMBEROFCREATEDTEXTURES))]

#planet should not have any child elements so we will just return it
    return planet


def ParseStarElement(root):
    star = Star()
    try: star.name = root.find("name").text
    except: pass
    
    try:star.mass = root.find("mass").text
    except: pass
    
    try: star.radius = float(root.find("radius").text)
    except: pass
    
    try: star.temperature = root.find("temperature").text
    except:pass
    
    try: star.spectraltype = root.find("spectraltype").text
    except:
        if star.temperature is not None:
            temp = star.temperature
            if(temp < 3500):
                star.spectraltype = "M"
            elif(temp >= 3500 or temp < 5000):
                star.spectraltype = "K"
            elif(temp >= 5000 or temp <6000):
                star.spectraltype = "G"
            elif(temp >= 6000 or temp <7500):
                star.spectraltype = "F"
            elif(temp >= 7500 or temp <11000):
                star.spectraltype = "A"
            elif(temp >= 11000 or temp <25000):
                star.spectraltype = "B"
            elif(temp >= 25000):
                star.spectraltype = "O"
            else:
                star.spectraltype = "G"
        else:
            star.spectraltype = "G"

    try: star.day = float(root.find("day").text)
    except:pass
    
    try: star.tilt = float(root.find("tilt").text)
    except:pass
            
    print "Creating Star " + star.name
    #find all children planets
    planets = root.findall("planet")
    for planet in planets:
        planetObject = ParsePlanetElement(planet)
        planetObject.parentNode = star
        print "Adding Child Planet " + planetObject.name
        star.planets.append(planetObject)
    
    return star


def ParseBinaryElement(root):
    binary = Binary()
    
    try: binary.semimajoraxis = root.find("semimajoraxis").text
    except:pass
    
    try: binary.eccentricity = root.find("eccentricity").text
    except: pass
    
    try: binary.periastron = root.find("periastron").text
    except: pass
    
    try: binary.longtitude = root.find("longitude").text
    except: pass
    
    try: binary.period = root.find("period").text
    except:pass
    
    print "Creating Binary "

    planets = root.findall("planet")
    for planet in planets:
        planetObject = ParsePlanetElement(planet)
        planetObject.parentNode = binary
        print "Found a binary child planet " + planetObject.name
        binary.planets.append(planetObject)
    
    stars = root.findall("star")
    for star in stars:
        starObject = ParseStarElement(star)
        starObject.parentNode = binary
        print "Found a binary child star " + starObject.name
        binary.stars.append(starObject)
    
    return binary

    

#ParseAll("./system_data")