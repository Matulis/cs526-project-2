import os
import glob


from math import *
from euclid import *
from omega import *
from cyclops import *

from omegaToolkit import *


PlanetTextures = {} #dict of all planet textures
StarTextures = {}
StarSphereTextures = {}

NUMBEROFCREATEDTEXTURES = 12

#Structure to hold a set of textures
class TextureSet():
    def __init__(self):
        self.sphereTexture = None
        self.multiplesTexture = None
        self.name = None


def LoadAllPlanetTextures(filepath_sphere,filepath_multiples):
    
    filenames = glob.glob(filepath_sphere+"/*.*")
#Load sphere textures
    for filename in filenames:
        basename = os.path.basename(filename)
        name = os.path.splitext(basename)[0]
         
        textureSet = TextureSet()
        textureSet.name = name
        textureSet.sphereTexture = filename
        PlanetTextures[name] = textureSet
#Repeat for small multiples, doing it this way so file extension doesnt matter
#Even though code replication is yucky =/
    
    filenames = glob.glob(filepath_multiples+"/*.*")
    for filename in filenames:
        basename = os.path.basename(filename)
        name = os.path.splitext(basename)[0]
        PlanetTextures[name].multiplesTexture = loadImage(filename)




def LoadStarTextures(filepath):
    filenames = glob.glob(filepath+"/*.*")
    for filename in filenames:
        basename = os.path.basename(filename)
        name = os.path.splitext(basename)[0]
        print name
        starTexture = loadImage(filename)
        StarTextures[name] = starTexture
        StarSphereTextures[name] = filename

